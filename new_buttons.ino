#define COLS 3
#define ROWS 4

int rows[ROWS] = {2, 3, 4, 5};
int cols[COLS] = {6, 7, 8};


char btns[ROWS][COLS] = {
  {'1', '2', '3'},
  {'4', '5', '6'},
  {'7', '8', '9'},
  {'*', '0', '#'}
};

bool states[ROWS][COLS] = {
  {false, false, false},
  {false, false, false},
  {false, false, false},
  {false, false, false}
};

bool driblings[ROWS][COLS] = {
  {false, false, false},
  {false, false, false},
  {false, false, false},
  {false, false, false}
};



void setup() {
  Serial.begin(9600);
  for (int i = 0; i < ROWS; i++){
    pinMode(rows[i], INPUT);
  }
  for (int i = 0; i < COLS; i++){
    pinMode(cols[i], INPUT);
  }
  
  Serial.println("BOARD LOADED!");
}



void loop() {
  for (int i = 0; i < ROWS; i++){
    pinMode(rows[i], OUTPUT);
    digitalWrite(rows[i], HIGH);
    delay(1);
    for (int j = 0; j < COLS; j++){
      bool state = digitalRead(cols[j]);
      if (state != driblings[i][j]){
        driblings[i][j] = state;
        continue;
      }
      driblings[i][j] = state;
      if (state && !states[i][j]){
        Serial.print(btns[i][j]);
      }
      states[i][j] = state;
    }
    digitalWrite(rows[i], LOW);
    pinMode(rows[i], INPUT);
    delay(1);
  }
}
